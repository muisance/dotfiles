{-# OPTIONS_GHC -Wno-deprecations #-}

import System.Directory
import Data.Tree
import XMonad.Actions.TreeSelect
import System.Exit (exitSuccess)
import System.IO (hPutStrLn)
import XMonad
import qualified XMonad.StackSet as W
import XMonad.Actions.CopyWindow
  ( copyToAll
  , kill1
  , killAllOtherCopies
  , runOrCopy
  )
import XMonad.Actions.CycleWS as CWS (Direction1D(..), moveTo, shiftTo, WSType(..), nextScreen, prevScreen)
import qualified XMonad.Actions.CycleRecentWS as CRWS
import qualified XMonad.Actions.DynamicWorkspaceOrder as DO
import XMonad.Actions.GridSelect
import XMonad.Actions.Minimize
import XMonad.Actions.MouseResize
import XMonad.Actions.Promote
import XMonad.Actions.DynamicWorkspaces
import XMonad.Actions.DynamicWorkspaceGroups
import XMonad.Actions.RotSlaves (rotAllDown, rotSlavesDown)
import qualified XMonad.Actions.Search as S
import qualified XMonad.Actions.TreeSelect as TreeSel
import XMonad.Actions.TreeSelect (tsDefaultConfig, treeselectWorkspace, toWorkspaces)
import XMonad.Actions.WindowGo (runOrRaise)
import XMonad.Actions.WithAll (killAll, sinkAll)
import Data.Char (isSpace, toUpper)
import qualified Data.Map as M
import qualified Data.Map.Strict as Map
import Data.Maybe (fromJust, isJust)
import Data.Monoid
import XMonad.Hooks.DynamicLog
  ( PP(..)
  , dynamicLogWithPP
  , shorten
  , statusBar
  , wrap
  , xmobarColor
  , xmobarPP
  )
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.FadeInactive
import XMonad.Hooks.InsertPosition
import XMonad.Hooks.ManageDocks
  ( ToggleStruts(..)
  , avoidStruts
  , docksEventHook
  , manageDocks
  )
import XMonad.Hooks.ManageHelpers
  ( doCenterFloat
  , doFullFloat
  , isDialog
  , isFullscreen
  , transience'
  )
import XMonad.Hooks.Minimize
import XMonad.Hooks.Place
import XMonad.Hooks.ServerMode
import XMonad.Hooks.SetWMName
import XMonad.Hooks.UrgencyHook hiding (Never)
import XMonad.Hooks.WorkspaceByPos
import XMonad.Hooks.WorkspaceHistory
import XMonad.Hooks.XPropManage
import qualified XMonad.Layout.BoringWindows as B
import XMonad.Layout.CenteredMaster
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.Hidden
import XMonad.Layout.Master
import XMonad.Layout.Minimize
import XMonad.Layout.PerWorkspace
import XMonad.Layout.ResizableTile
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spiral
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns
import XMonad.Layout.Fullscreen (fullscreenFull, fullscreenSupport)
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows (decreaseLimit, increaseLimit, limitWindows)
import XMonad.Layout.Magnifier
import XMonad.Layout.MultiToggle (EOT(EOT), (??), mkToggle, single)
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))
import XMonad.Layout.MultiToggle.Instances
  ( StdTransformers(MIRROR, NBFULL, NOBORDERS)
  )
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed
import XMonad.Layout.ShowWName
import XMonad.Layout.Simplest
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import qualified XMonad.Layout.ToggleLayouts as T
  ( ToggleLayout(Toggle)
  , toggleLayouts
  )
import XMonad.Layout.WindowArranger (WindowArrangerMsg(..), windowArrange)
import XMonad.Layout.WindowNavigation
import Control.Arrow (first)
import XMonad.Prompt
import XMonad.Prompt.FuzzyMatch
import XMonad.Prompt.Input
import XMonad.Prompt.Man
import XMonad.Prompt.Pass
import XMonad.Prompt.Shell
import XMonad.Prompt.Ssh
import XMonad.Prompt.XMonad
import Text.Printf
import XMonad.Util.ClickableWorkspaces (clickablePP)
import XMonad.Util.Dmenu
import XMonad.Util.EZConfig (additionalKeysP, additionalMouseBindings)
import XMonad.Util.NamedScratchpad
import XMonad.Util.Run (runProcessWithInput, safeSpawn, spawnPipe)
import XMonad.Util.SpawnOnce

myFont :: String
myFont =
  "xft:Cascadia Code PL:pixelsize=13:antialias=true:hinting=true"

myBordWdth :: Dimension
myBordWdth = 1 -- Sets border width for windows

myNormBordCol :: String
myNormBordCol = "#0093fc" -- Border color of normal windows

myFocusBordCol :: String
myFocusBordCol = "#11FF88" -- Border color of focused windows

myModMask :: KeyMask
myModMask = mod4Mask -- Sets modkey to super/windows key

altMask :: KeyMask
altMask = mod1Mask -- Setting this for use in xprompts

-- |whether clicking on a window to focus also passes the click to the window
myClickJustFocuses :: Bool
myClickJustFocuses = True

myTerm :: String
myTerm = "alacritty"

myAltTerm :: String
myAltTerm = "urxvt"

mySt :: String
mySt = "st"

myBrowser :: String
myBrowser = "chromium" -- Sets firefox as browser for tree select

myEditor :: String
myEditor = myTerm ++ " -e nvim " -- Sets nvim as editor for tree select

windCount :: X (Maybe String)
windCount =
  gets $
  Just .
  show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

myStartupHook :: X ()
myStartupHook = do
  spawnOnce
    "trayer --edge top --align right --widthtype request --padding 0 --SetDockType true --SetPartialStrut true --expand true --monitor 0 --transparent true --height 30 &"
  spawnOnce "/usr/bin/emacs --daemon &"
  spawnOnce "volumeicon &"
  spawnOnce "nitrogen --restore &"
  spawnOnce "picom &"
  spawnOnce "nm-applet &"
  setWMName "LG3D"

myColorizer :: Window -> Bool -> X (String, String)
myColorizer =
  colorRangeFromClassName
    (0x28, 0x2c, 0x34) -- lowest inactive bg
    (0x28, 0x2c, 0x34) -- highest inactive bg
    (0xc7, 0x92, 0xea) -- active bg
    (0xc0, 0xa7, 0x9a) -- inactive fg
    (0x28, 0x2c, 0x34) -- active fg

mygridConfig :: p -> GSConfig Window
mygridConfig colorizer =
  (buildDefaultGSConfig myColorizer)
    { gs_cellheight = 40
    , gs_cellwidth = 160
    , gs_cellpadding = 6
    , gs_originFractX = 0.5 -- default: {0.5 | cent-scr} // {0.1 | left-top} / {1.0 | rght-bot}
    , gs_originFractY = 0.5 -- default: {0.5 | cent-scr} // {0.1 | left-top} / {1.0 | rght-bot}
    , gs_font = myFont
    }

spawnSelected' :: [(String, String)] -> X ()
spawnSelected' lst = gridselect conf lst >>= flip whenJust spawn
  where
    conf =
      def
        { gs_cellheight = 40
        , gs_cellwidth = 160
        , gs_cellpadding = 6
        , gs_originFractX = 0.5 -- default: {0.5 | cent-scr} // {0.1 | left-top} / {1.0 | rght-bot}
        , gs_originFractY = 0.5 -- default: {0.5 | cent-scr} // {0.1 | left-top} / {1.0 | rght-bot}
        , gs_font = myFont
        }

myAppGrid :: [([Char], [Char])]
myAppGrid =
  [ ("Code Insiders", "code-insiders")
  , ("Brave", "brave")
  , ("Firefox", "firefox")
  , ("Vivaldi", "vivaldi-stable")
  , ("VirtualBox Manager", "VirtualBox Manager")
  , ("Gnome-boxes", "gnome-boxes")
  , ("Virt-Manager", "Virt-Manager")
  , ("Slack", "slack-desktop")
  , ("Signal", "signal-desktop")
  , ("Telegram", "telegram-desktop")
  , ("Element", "element-desktop")
  , ("Jami", "jami-gnome")
  , ("Pcmanfm", "pcmanfm")
  ]

 --tsDefaultConfig = TSConfig { ts_hidechildren = True
 --                          , ts_background   = 0xc0c0c0c0
 --                          , ts_font         = "xft:Iosevka Term-13"
 --                          , ts_node         = (0xff000000, 0xff50d0db)
 --                          , ts_nodealt      = (0xff000000, 0xff10b8d6)
 --                          , ts_highlight    = (0xffffffff, 0xffff0000)
 --                          , ts_extra        = 0xff000000
 --                          , ts_node_width   = 200
 --                          , ts_node_height  = 30
 --                          , ts_originX      = 0
 --                          , ts_originY      = 0
 --                          , ts_indent       = 80
 --                          , ts_navigate     = myNavigation
 --                          }

--treeselectAction :: TSConfig (X a) -> Forest (TSNode (X a)) -> X ()
--treeselectAction = myTreeConf
 --  def
 --     [ Node (TSNode "System"		  "actions on the system"               (return ()))
 --         [ Node (TSNode "Restart PC"       "reboots the system"                (spawn "reboot"))   []
 --         , Node (TSNode "Shutdown"         "Poweroff the system"               (spawn "shutdown")) []
 --         ]
 --     , Node (TSNode "Brightness"		  "Sets screen brightness using xbacklight"       (return ()))
 --         [ Node (TSNode "Bright"           "FULL POWER!!"                      (spawn "xbacklight -set 100")) []
 --         , Node (TSNode "Normal"           "Normal Brightness (50%)"           (spawn "xbacklight -set 50"))  []
 --         , Node (TSNode "Dim"              "Quite dark"                        (spawn "xbacklight -set 10"))  []
 --         ]
 --     , Node (TSNode "Code"		"Code Editors & IDEs"	          (return ()))
 --         [ Node (TSNode "VSCode"	         "MS Visual Studio Code for Linux"	  (spawn "code"))           []
 --         , Node (TSNode "VSCode Insiders"  "VSC, but a slightly more unstable"  (spawn "code-insiders")) []
 --         , Node (TSNode "Emacs"            "E"                                  (spawn "emacs"))         []
 --         ]   
 --     ]

dtXPConfig :: XPConfig
dtXPConfig =
  def
    { font = myFont
    , bgColor = "#282c34"
    , fgColor = "#bbc2cf"
    , bgHLight = "#c792ea"
    , fgHLight = "#000000"
    , borderColor = "#282c34"
    , promptBorderWidth = 0
    , promptKeymap = dtXPKeymap
    , position = Top
    , height = 30
    , historySize = 512
    , historyFilter = id
    , defaultText = []
    , autoComplete = Just 100000 -- set Just 100000 for .1 sec
    , showCompletionOnTab = False
    , searchPredicate = fuzzyMatch
    , defaultPrompter = map toUpper -- change prompt to UPPER
    , alwaysHighlight = True
    , maxComplRows = Just 1 -- used to be 'Nothing'     -- set to 'Just 5' for 5 rows
    }

dtXPConfig' :: XPConfig
dtXPConfig' = dtXPConfig {autoComplete = Nothing}

promptList :: [(String, XPConfig -> X ())]
promptList =
  [ ("m", manPrompt) -- manpages prompt
  , ("p", passPrompt) -- get passwords (requires 'pass')
  , ("g", passGeneratePrompt) -- generate passwords (requires 'pass')
  , ("r", passRemovePrompt) -- remove passwords (requires 'pass')
  , ("s", sshPrompt) -- ssh prompt
  , ("x", xmonadPrompt) -- xmonad prompt
  ]

dtXPKeymap :: M.Map (KeyMask, KeySym) (XP ())
dtXPKeymap =
  M.fromList $
  map
    (first $ (,) controlMask) -- control + <key>
    [ (xK_z, killBefore) -- kill line backwards
    , (xK_k, killAfter) -- kill line forwards
    , (xK_a, startOfLine) -- move to the beginning of the line
    , (xK_e, endOfLine) -- move to the end of the line
    , (xK_m, deleteString Next) -- delete a character foward
    , (xK_b, moveCursor Prev) -- move cursor forward
    , (xK_f, moveCursor Next) -- move cursor backward
    , (xK_BackSpace, killWord Prev) -- kill the previous word
    , (xK_y, pasteString) -- paste a string
    , (xK_g, quit) -- quit out of prompt
    , (xK_bracketleft, quit)
    ] ++
  map
    (first $ (,) altMask) -- meta key + <key>
    [ (xK_BackSpace, killWord Prev) -- kill the prev word
    , (xK_f, moveWord Next) -- move a word forward
    , (xK_b, moveWord Prev) -- move a word backward
    , (xK_d, killWord Next) -- kill the next word
    , (xK_n, moveHistory W.focusUp') -- move up thru history
    , (xK_p, moveHistory W.focusDown') -- move down thru history
    ] ++
  map
    (first $ (,) 0) -- <key>
    [ (xK_Return, setSuccess True >> setDone True)
    , (xK_KP_Enter, setSuccess True >> setDone True)
    , (xK_BackSpace, deleteString Prev)
    , (xK_Delete, deleteString Next)
    , (xK_Left, moveCursor Prev)
    , (xK_Right, moveCursor Next)
    , (xK_Home, startOfLine)
    , (xK_End, endOfLine)
    , (xK_Down, moveHistory W.focusUp')
    , (xK_Up, moveHistory W.focusDown')
    , (xK_Escape, quit)
    ]

mySpacing ::
     Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

mySpacing' ::
     Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing' i = spacingRaw True (Border i i i i) True (Border i i i i) True

tall =
  renamed [Replace "[tL]"] $
  minimize $
  smartBorders $
  windowNavigation $
  addTabs shrinkText myTabTheme $
  subLayout [] (smartBorders Simplest) $
  limitWindows 7 $ mySpacing 2 $ ResizableTall 1 (3 / 100) (1 / 2) []

floats =
  renamed [Replace "[fL]"] $
  minimize $ smartBorders $ limitWindows 6 simplestFloat

spirals =
  renamed [Replace "[Sp]"] $
  minimize $
  smartBorders $
  windowNavigation $
  magnifiercz 1.3 $
  addTabs shrinkText myTabTheme $
  subLayout [] (smartBorders Simplest) $
  limitWindows 8 $ mySpacing 6 $ spiral (6 / 7)

threeCol =
  renamed [Replace "[3c]"] $
  minimize $
  smartBorders $
  windowNavigation $
  magnifiercz 1.1 $
  addTabs shrinkText myTabTheme $
  subLayout [] (smartBorders Simplest) $
  limitWindows 7 $ mySpacing 4 $ ThreeCol 1 (3 / 100) (1 / 2)

tabs = renamed [Replace "[tB]"] $ minimize $ tabbed shrinkText myTabTheme

myTabTheme =
  def
    { fontName = myFont
    , activeColor = "#46D9FF"
    , inactiveColor = "#313846"
    , activeBorderColor = "#46D9FF"
    , inactiveBorderColor = "#282C34"
    , activeTextColor = "#282C34"
    , inactiveTextColor = "#D0D0D0"
    }

myShowWNameTheme :: SWNConfig
myShowWNameTheme =
  def
    { swn_font =
        "xft:Font Awesome 5 Brands Regular:size=60:antialias=true:hinting=true"
    , swn_fade = 1.0
    , swn_bgcolor = "#0C020D"
    , swn_color = "#FFFFFF"
    }

--Minimizing windows
minWin = withFocused minimizeWindow <+> spawn "XMMWO"
restWin = withLastMinimized maximizeWindowAndFocus <+> spawn "XMMWC"

--restWin          =  sendMessage RestoreNextMinimizedWin <+> spawn "XMMWC"
myLayoutHook =
  avoidStruts $
  mouseResize $
  windowArrange $
  hiddenWindows $
  T.toggleLayouts floats $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
  where
    myDefaultLayout =
      withBorder myBordWdth tall |||
      withBorder myBordWdth spirals |||
      withBorder myBordWdth threeCol |||
      noBorders tabs ||| noBorders floats ||| noBorders Full

myWorkspaces :: [String]
myWorkspaces =
  zipWith
    (<>)
    (map ((<> " ") . show) [(1 :: Int) ..])
    [ "\xf3d3" -- | :1 <<< Node logo || "\xf1cb" -- | :1 <<< HackerRank Logo
    , "\xf269" -- | :2 <<< Firefox Logo
    , "\xf7c6" -- | :3 <<< Sketch Logo
    , "\xf167" -- | :4 <<< YouTube Logo
    , "\xf395" -- | :5 <<< Docker Logo || "\xf841" -- | :5 <<< Git Logo
    , "\xf202" -- | :6 <<< LastFM Logo
    , "\xf3fe" -- | :7 <<< Telegram Logo
    , "\xf3d0" -- | :8 <<< Monero Logo || "\xf1b6" -- | :8 <<< Steam Logo
    , "\xf40d" -- | :9 <<< Some Gears Logo
    ]

myWorkspaceIndices = Map.fromList $ zip myWorkspaces [(1 :: Int) ..] -- (,) == \x y -> (x,y)

clickable ws =
  "<action=xdotool key super+" ++ show i ++ ">" ++ ws ++ "</action>"
  where
    i = fromJust $ Map.lookup ws myWorkspaceIndices

myManageHook :: XMonad.Query (Data.Monoid.Endo WindowSet)
myManageHook =
  composeAll
    [ className =? "mpv" --> doShift (myWorkspaces !! 3)
    , className =? "moc" --> doShift (myWorkspaces !! 5)
    , className =? "ncmpcpp" --> doShift (myWorkspaces !! 5)
    , className =? "nuclear" --> doShift (myWorkspaces !! 5)
    , className =? "Signal" --> doShift (myWorkspaces !! 6)
    , className =? "TelegramDesktop" --> doShift (myWorkspaces !! 6)
    , className =? "Element-Desktop" --> doShift (myWorkspaces !! 6)
    , className =? "slack-desktop" --> doShift (myWorkspaces !! 6)
    , className =? "Signal" --> doShift (myWorkspaces !! 6)
    , className =? "Gimp" --> doShift (myWorkspaces !! 2)
    , className =? "VirtualBox Manager" --> doShift (myWorkspaces !! 4)
    , className =? "VirtualBox Machine" --> doShift (myWorkspaces !! 4)
    , className =? "VirtualBoxVM" --> doShift (myWorkspaces !! 4)
    , className =? "Virt-Manager" --> doShift (myWorkspaces !! 4)
    , className =? "Gnome-boxes"  --> doShift (myWorkspaces !! 4)
    , className =? "uxterm" --> doFloat
    , className =? "UXterm" --> doFloat
    , className =? "urxvt" --> doFloat
    , className =? "rxvt" --> doFloat
    , className =? "rxvt-unicode" --> doFloat
    , className =? "st" --> doFloat
    , resource =? "desktop_window" --> doIgnore
    , resource =? "desktop_window" --> doFloat -- Float dialogue windows
    , className =? "mpv" --> doFullFloat
    , className =? "moc" --> doFullFloat
    , className =? "Yad" --> doFloat
    , className =? "ncmpcpp" --> doFloat
    , className =? "Virt-Manager" --> doFloat
    , className =? "VirtualBox Manager" --> doFloat
    , className =? "VirtualBox Machine" --> doFloat
    , className =? "Gnome-boxes"  --> doFloat
    , className =? "VirtualBoxVM" --> doFloat
    , className =? "st-256color" --> doCenterFloat
    , className =? "ncmpcpp" --> doCenterFloat
    , isDialog --> doCenterFloat
    , transience'
    , (className =? "brave" <&&> resource =? "Dialog") --> doFloat
    , (className =? "firefox" <&&> resource =? "Dialog") --> doFloat
    , (className =? "chromium" <&&> resource =? "Dialog") --> doFloat
    , (className =? "vivaldi-bin" <&&> resource =? "Dialog") --> doFloat
    , (className =? "vivaldi-stable" <&&> resource =? "Dialog") --> doFloat
    , isFullscreen --> doFullFloat
    ] 

myLogHook :: X ()
myLogHook = fadeInactiveLogHook fadeAmount
  where
    fadeAmount = 1.0

main :: IO ()
main = do
  xmproc <- spawnPipe "xmobar /home/neko_ill/.xmonad/xmobarrc"
  xmonad $
    withUrgencyHook NoUrgencyHook $
    ewmh
      def
        { manageHook =
            (isFullscreen --> doFullFloat) <+> myManageHook <+> manageDocks
        , handleEventHook =
            serverModeEventHookCmd <+>
            serverModeEventHook <+>
            serverModeEventHookF "XMONAD_PRINT" (io . putStrLn) <+>
            docksEventHook <+> minimizeEventHook
        , modMask = myModMask
        , terminal = myTerm
        , startupHook = myStartupHook
        , layoutHook = showWName' myShowWNameTheme myLayoutHook
        , workspaces = myWorkspaces
        , borderWidth = myBordWdth
        , normalBorderColor = myNormBordCol
        , focusedBorderColor = myFocusBordCol
        , logHook =
            workspaceHistoryHook <+>
            myLogHook <+>
            dynamicLogWithPP
              xmobarPP
                { ppOutput  = System.IO.hPutStrLn xmproc
                , ppCurrent = xmobarColor "#0C020D" "#F1F900" . wrap "<box><fn=6> " " </fn></box>"
                , ppVisible = xmobarColor "#15FF00" "" . wrap "" "" . clickable
                , ppHidden  = xmobarColor "#FC3FAF" "" . wrap "" "" . clickable
                , ppHiddenNoWindows = xmobarColor "#193B47" "" . wrap "<fn=3>" "</fn>" . clickable
                , ppSep     = "<fc=#44BBFF><fn=1> : </fn></fc>"
                , ppWsSep   = "<fc=#193B47><fn=1> :</fn></fc>"
                , ppUrgent  = xmobarColor "#FC902F" "" . wrap "<fn=1>!</fn>" "" . clickable
                , ppExtras  = [windCount]
                , ppLayout  = wrap "<fc=#9799FF><fn=1> " " </fn></fc>"
                , ppTitle   = wrap "<fc=#0C0209,#44BBFF><fn=1> " " </fn></fc>" . shorten 40
                , ppOrder   = \(ws:t:l:ex) -> [ws,t,l]++ex
                }
        } `additionalKeysP`
    myKeys

myKeys :: [(String, X ())]
myKeys =
  [ ("M-C-r", spawn "xmonad --recompile") -- Recompiles xmonad
  , ("M-S-r", spawn "xmonad --restart") -- Restarts xmonad
  , ("M-S-q", io exitSuccess) -- Quits xmonad
  , ("M-<Return>", shellPrompt dtXPConfig) -- Shell Prompt
  , ("M-S-p", spawn "dmenu_run -i -p \"Run: \"") -- Dmenu
  , ("M-S-o", spawn "rofi -show window")
  , ("M-C-o", spawn "rofimoji")
  , ("M-M1-o", spawn "rofi -show run")
  , ("M-C-m", manPrompt dtXPConfig) -- manPromptCenter
  , ("M-C-p", passPrompt dtXPConfig) -- passPrompt
  , ("M-C-g", passGeneratePrompt dtXPConfig) -- passGeneratePrompt
  , ("M-M1-g", passRemovePrompt dtXPConfig) -- passRemovePrompt
  , ("M-M1-s", sshPrompt dtXPConfig) -- sshPrompt
  , ("M-M1-p", xmonadPrompt dtXPConfig) -- xmonadPrompt
  , ("M-e", spawn myTerm)                                -- | spawns default terminal (specified in `myTerm`)
  , ("M-S-e", spawn myAltTerm)                           -- | spawns backup terminal (specified in `myAltTerm`)
  , ("M-C-e", spawn mySt)
  , ("M-q", spawn "urxvt")
  , ("M-w", spawn "xterm")
  , ("M-S-b", spawn myBrowser)                           -- | spawns specified browser
  , ("M-M1-t", spawn "telegram-desktop")                 -- | Telegram
  , ("M-x", kill1)                                       -- | Kill the currently focused window
  , ("M-S-x", killAll)                                   -- | Kill all windows on current workspace
  , ("M-C-s", windows copyToAll)
  , ("M-C-a", killAllOtherCopies)
  , ("M-M1-m", withFocused hideWindow)                   -- | Minimize Window
  , ("M-M1-l", popOldestHiddenWindow)                    -- | Restore Oldest Hidden Window
  , ("M-M1-r", popNewestHiddenWindow)                    -- | Restore Newest Hidden Window
  , ("M-.", nextScreen)                                  -- | Focus to next monitor for multi-monitor setup
  , ("M-,", prevScreen)                                  -- | Focus to prev monitor  for multi-monitor setup
  , ("M-n", CWS.moveTo Next nonNSP)
  , ("M-b", CWS.moveTo Prev nonNSP)
  , ("M-S-,", CWS.shiftTo Prev nonNSP >> CWS.moveTo Prev nonNSP)
  , ("M-S-.", CWS.shiftTo Next nonNSP >> CWS.moveTo Next nonNSP)
  , ("M-f", sendMessage (T.Toggle "floats"))             -- | Toggles my 'floats' layout
  , ("M-t", withFocused $ windows . W.sink)              -- | Push floating window back to tile
  , ("M-S-t", sinkAll)                                   -- | Push ALL floating windows to tile
  , ("M-d", decWindowSpacing 2)                          -- | Decrease window spacing
  , ("M-i", incWindowSpacing 2)                          -- | Increase window spacing
  , ("M-S-d", decScreenSpacing 2)                        -- | Decrease screen spacing
  , ("M-S-i", incScreenSpacing 2)                        -- | Increase screen spacing
  , ("C-g g", spawnSelected' myAppGrid)                  -- | grid select favorite apps
  , ("C-g t", goToSelected $ mygridConfig myColorizer)   -- | goto selected window
  , ("C-g b", bringSelected $ mygridConfig myColorizer)  -- | bring selected window
  , ("M-m", windows W.focusMaster)                                    -- Move focus to the master window
  , ("M-j", windows W.focusDown) -- Move focus to the next window
  , ("M-k", windows W.focusUp) -- Move focus to the prev window
  , ("M-S-m", windows W.swapMaster) -- Swap the focused window and the master window
  , ("M-S-j", windows W.swapDown) -- Swap focused window with next window
  , ("M-S-k", windows W.swapUp) -- Swap focused window with prev window
  , ("M-<Backspace>", promote) -- Moves focus to master, others maintain order
  , ("M-S-<Tab>", rotSlavesDown) -- Rotate all except master & keep focus in place
  , ("M-C-<Tab>", rotAllDown) -- Rotate all the windows in the current stack
  , ("M-<Tab>", sendMessage NextLayout) -- Switch to next layout
  , ("M-C-<Up>", sendMessage Arrange)
  , ("M-C-<Down>", sendMessage DeArrange)
  , ("M-<Space>", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts) -- Toggles noborder/full
  , ("M-S-<Space>", sendMessage ToggleStruts) -- Toggles struts
  , ("M-S-n", sendMessage $ MT.Toggle NOBORDERS) -- Toggles noborder
  , ("M-S-<Up>", sendMessage (IncMasterN 1)) -- Increase number of clients in master pane
  , ("M-S-<Down>", sendMessage (IncMasterN (-1))) -- Decrease number of clients in master pane
  , ("M-C-<Up>", increaseLimit) -- Increase number of windows
  , ("M-C-<Down>", decreaseLimit) -- Decrease number of windows
  , ("M-h", sendMessage Shrink) -- Shrink horiz window width
  , ("M-l", sendMessage Expand) -- Expand horiz window width
  , ("M-M1-j", sendMessage MirrorShrink) -- Shrink vert window width
  , ("M-M1-k", sendMessage MirrorExpand) -- Exoand vert window width
  , ("M-C-h", sendMessage $ pullGroup L)
  , ("M-C-l", sendMessage $ pullGroup R)
  , ("M-C-k", sendMessage $ pullGroup U)
  , ("M-C-j", sendMessage $ pullGroup D)
  , ("M-C-m", withFocused (sendMessage . MergeAll))
  , ("M-C-/", withFocused (sendMessage . UnMergeAll))
  , ("M-C-.", onGroup W.focusUp') -- Switch focus to next tab
  , ("M-C-,", onGroup W.focusDown') -- Switch focus to prev tab
  , ("<XF86AudioLowerVolume>", spawn "amixer set Master 5%- unmute")
  , ("<XF86AudioRaiseVolume>", spawn "amixer set Master 5%+ unmute")
  , ("<XF86HomePage>", spawn "brave")
  , ("<XF86Search>", safeSpawn "brave" ["https://www.duckduckgo.com/"])
  ]
   ++
  [("M-p " ++ k, f dtXPConfig') | (k, f) <- promptList]
          where nonNSP          = WSIs (return (\ws -> W.tag ws /= "NSP"))
                nonEmptyNonNSP  = WSIs (return (\ws -> isJust (W.stack ws) && W.tag ws /= "NSP"))
